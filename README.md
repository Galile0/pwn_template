# Summary
This is a simple skeleton generator for binary exploitation with pwntools

# Requirements
* pip install jinja2
* pip install pwntools
* apt install patchelf

# How to use
Simply Call generator.py  
The following features are supported:  
* Patch the Binary to use a special dynamic Interpreter  
    Depending on the system you are working on and the libc of the remote target this may be neccessary for developing heap based exploits  
* Supply gdb with a custom directory for Debug Symbols  
    Sometimes during a CTF a custom LIBC is provided. Using this feature you can point GDB to a directory containing the libc debug symbols (or any other library debug symbols for that matter). It's somewhat buggy and recommended that you load it into your .gdbinit
