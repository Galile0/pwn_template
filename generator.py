#!/usr/bin/env python3
import argparse
import sys
import os
from pwn import *
from jinja2 import Environment, FileSystemLoader

def get_path(file, absolute=False):
    if os.path.isabs(file) and absolute:
        ret = file
    elif absolute:
        ret = os.path.join(os.getcwd(), file)
    else:
        ret = os.path.relpath(file, os.getcwd())
    return ret

if __name__ == '__main__':
    parser = argparse.ArgumentParser("Create a Skeleton for Binary Exploitation with pwntools\n")
    parser.add_argument("binary", help="Path to target binary")
    parser.add_argument("-o", "--output", metavar='Output File', 
                        help="Output file for generated skeleton. Leave empty to print to stdout")
    parser.add_argument("-t", "--target", metavar='Remote Host',
                        help="Target for remote exploitation")
    parser.add_argument("-p", "--port", metavar='Remote Port',
                        help="Port for remote exploitation")
    parser.add_argument("-ld", "--ld", metavar='Dynamic Interpreter to match libc',
                        help="File to be used for custom dynamic loader")
    parser.add_argument("-lc", "--libc", metavar='Libc to be used for program',
                        help="Set libc to use for the binary. Loaded via LD_Preload on startup")
    parser.add_argument("-s", "--staging", action='store_true', dest='staging',
                        help="Add Staging environment to exploit script to separate between local libc and remote libc.")
    parser.add_argument("-c", "--convenience", action='store_true', dest='convenience',
                        help="Add convenience functions like ru(), sl(), irt() etc.")
    parser.add_argument("-e", "--errorhandler", action='store_true', dest='errorhandler',
                        help="Add errorhandler @atexception to print Data when your pipe closes")
    parser.add_argument("-g", "--gdblauncher", action='store_true', dest='gdblauncher',
                        help="Add launch_gdb function to ease usage of gdb commands and setting breakpoints")
    parser.add_argument("-ap", "--absolutepaths", action='store_true', dest='absolutepaths',
                        help="Enable absolute paths instead of relative paths")
    parser.set_defaults(staging=False, convenience=False, errorhandler=False, gdblauncher=False, absolutepaths=False)
    args = parser.parse_args()

    context.log_level = 'info'

    info("Starting Template Generator")
    config_dict = {}
    binary = ELF(args.binary, checksec=False)

    config_dict['binary'] = "ELF('" + get_path(binary.path, args.absolutepaths) + "')"
    config_dict['host'] = "'" + args.target + "'" if args.target else "'" + '<INSERT REMOTE IP>' + "'"
    config_dict['port'] = args.port if args.port else 1337
    config_dict['pie'] = binary.pie
    if binary.statically_linked:
        info("Binary is statically linked")
        if args.libc:
            error("Can't preload LIBC with statically linked binary")
    elif binary.libc:
        info("Setting up local LIBC")
        config_dict['local_libc'] = "ELF('" + binary.libc.path + "', checksec=False)"
    else:
        info("Binary is hardcoded to not use global libc")

    if args.libc:
        info("Setting up Remote LIBC")
        config_dict['remote_libc'] = "ELF('" + get_path(args.libc, args.absolutepaths) + "', checksec=False)"
    elif not binary.statically_linked:
        info("Insert placeholder for remote LIBC")
        config_dict['remote_libc'] = "local_libc # ELF('TODO ADD REMOTE LIBC', checksec=False)"

    if args.ld:
        info("Setting up ld.so")
        config_dict['ld'] = "ELF('" + get_path(args.ld, args.absolutepaths)  + "', checksec=False)"

    info("Generating template")
    file_loader = FileSystemLoader('/')
    env = Environment(loader=file_loader)
    template = env.get_template(os.path.dirname(os.path.realpath(sys.argv[0]))+'/template.py')
    output = template.render(
                            config = config_dict,
                            binary_pie = binary.pie,
                            enable_staging = args.staging,
                            use_remote_libc = args.libc,
                            enable_convenience = args.convenience,
                            enable_errorhandler = args.errorhandler,
                            enable_gdblauncher = args.gdblauncher
                            )
    if args.output:
        i = 1
        save_file = args.output
        pos = args.output.rfind('.')
        while(os.path.isfile(save_file)):
            save_file = args.output[:pos] + '_' + str(i) + args.output[pos:]
            i += 1
        with open(save_file, "w") as f:
            f.write(output)
        os.chmod(save_file, 0b111101000)
        success("Saved Skeleton as: {}. Have fun Exploiting".format(save_file))

    else:
        print(output)
