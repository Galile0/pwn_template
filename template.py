#!/usr/bin/env python3
from pwn import *

{%- if enable_convenience %}
#convenience Functions
s       = lambda data               :io.send(data)
sa      = lambda delim,data         :io.sendafter(delim, data, timeout=context.timeout) 
sl      = lambda data               :io.sendline(data) 
sla     = lambda delim,data         :io.sendlineafter(delim, data, timeout=context.timeout) 
rl      = lambda numb=4096          :io.recvline(numb)
ru      = lambda delims, drop=True  :io.recvuntil(delims, drop, timeout=context.timeout)
uu32    = lambda data               :u32(data.ljust(4, b'\x00'))
uu64    = lambda data               :u64(data.ljust(8, b'\x00'))
{%- endif %}

# Exploit configs
{%- for key, value in config.items() %}
{{key}} = {{value}}
{%- endfor %}
{%- if enable_errorhandler %}

@atexception.register
def handler():
    if sys.last_type in [EOFError, struct.error, KeyboardInterrupt ]:
        data = io.stream()
        if data:
            warn("Connection got closed, last data received:")
            error(data)
        else:
            error("Connection got closed, no data pending")
{%- endif %}
{%- if enable_gdblauncher %}

def launch_gdb(breakpoints=[], cmds=[]):
    if args.NOPTRACE:
        return
    context.terminal = ['tmux', 'split-pane', '-h']
    info("Attaching Debugger")
    cmds.append('handle SIGALRM ignore')
    # cmds.append('set follow-fork-mode child')
    for b in breakpoints:
        {%- if config['local_libc'] and binary_pie%}
        if binary.address == 0:
            warning("Setting relative Breakpoints but binary has not been rebased")
        cmds.insert(0,'b *' + str(binary.address + b))
        {%- else %}
        cmds.insert(0,'b *' + str(b))
        {%- endif %}
    gdb.attach(io, gdbscript='\n'.join(cmds))
{%- endif %}

def demask(x, l=64):
    p = 0
    for i in range(l*4,0,-4): # 16 nibble
        v1 = (x & (0xf << i )) >> i
        v2 = (p & (0xf << i+12 )) >> i+12
        p |= (v1 ^ v2) << i
    return p

def mask(p, adr):
    return p^(adr>>12)

if __name__ == '__main__':
    # context.timeout = 1
    # call with DEBUG to change log level
    # call with NOPTRACE to skip gdb attach
    # call with REMOTE to run against live target

    if args.REMOTE:
        args.NOPTRACE = True # disable gdb when working remote
        io = remote(host, port)
        {%- if config['local_libc'] %}
        libc = remote_libc
        {%- endif %}
    {%- if enable_staging %}
    elif args.STAGING:
        {%- if config['ld'] and use_remote_libc %}
        io = process([ld.path, binary.path], env={'LD_PRELOAD': remote_libc.path})
        {%- elif config['ld'] %}
        io = process([ld.path, binary.path])
        {%- elif use_remote_libc %}
        io = process(binary.path, env={'LD_PRELOAD': remote_libc.path})
        {%- else %}
        io = process(binary.path)
        {%- endif %}
        {%- if config['local_libc'] %}
        libc = remote_libc
        {%- endif %}
    {%- endif %}
    else:
        {%- if enable_staging %}
        io = process(binary.path)
        {%- if config['local_libc'] %}
        libc = local_libc
        {%- endif %}
        {%- else %}
        {%- if config['ld'] and use_remote_libc %}
        io = process([ld.path, binary.path], env={'LD_PRELOAD': remote_libc.path})
        {%- elif config['ld'] %}
        io = process([ld.path, binary.path])
        {%- elif use_remote_libc %}
        io = process(binary.path, env={'LD_PRELOAD': remote_libc.path})
        {%- else %}
        io = process(binary.path)
        {%- endif %}
        {%- if use_remote_libc %}
        libc = remote_libc
        {%- elif config['local_libc'] %}
        libc = local_libc
        {%- endif %}
        {%- endif %}
    {%- if config['local_libc'] and binary_pie%}
    if not args.REMOTE:
        for mmap in open('/proc/{}/maps'.format(io.pid),"rb").readlines():
            mmap = mmap.decode()
            if binary.path.split('/')[-1] == mmap.split('/')[-1][:-1]:
                binary.address = int(mmap.split('-')[0],16)
                break
    {%- endif %}
    
    io.interactive()
