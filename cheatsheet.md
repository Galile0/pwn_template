# Loading local gdbinits
By settings these options in your ~/.gdbinit you can load additional gdbinit files from your project folder. Useful to automatically load custom options like memory watchpoints or breakpoints on a per project basis.
set auto-load local-gdbinit
set auto-load safe-path /
Enable this only if you know what you are doing. This could be abused to load unsafe init files into gdb. Or something. I dont know.